using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
	[SerializeField] private GameObject thisCamera;
	[SerializeField] private float cameraSpeed;
	private Camera cmr;
	
    void Start()
    {
        cmr = thisCamera.gameObject.GetComponent<Camera>();
    }

    void Update()
    {
        Vector2 direction = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
		thisCamera.transform.Translate(direction * cameraSpeed * Time.deltaTime);
		
		if(Input.GetKey(KeyCode.E))
		{
			cmr.orthographicSize -= cameraSpeed * Time.deltaTime;
			if(cmr.orthographicSize < 5)
				cmr.orthographicSize = 5f;
		}
		
		if(Input.GetKey(KeyCode.Q))
		{
			cmr.orthographicSize += cameraSpeed * Time.deltaTime;
			if(cmr.orthographicSize > 32)
				cmr.orthographicSize = 32f;
		}
    }
}
