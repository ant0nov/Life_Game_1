using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
	[SerializeField] private bool state;
	[SerializeField] private GameObject cell;
	[SerializeField] private float simulationSpeed;
	[SerializeField] private int width;
	[SerializeField] private int height;
	private SpriteRenderer[] srGreed;
	private Color[] newColor;
	
    void Start()
    {
		state = false;
		srGreed = new SpriteRenderer[width * height];
		newColor = new Color[width * height];
        for(int x = 0; x < width; x++)
			for(int y = 0; y < height; y++)
			{
				GameObject pref = Instantiate(cell, new Vector2(x,y), transform.rotation);
				srGreed[x + y * width] = pref.GetComponent<SpriteRenderer>();
			}
			
		float cameraX, cameraY;
		cameraX = Mathf.Round(width/2);
		cameraY = Mathf.Round(height/2);
		Camera.main.transform.position = new Vector3(cameraX, cameraY, -10f);
		Camera.main.orthographicSize = 6 + 4 * (Mathf.Max(width, height)) / 10;
		if (Camera.main.orthographicSize > 32) Camera.main.orthographicSize = 32;
    }

    void Update()
    {
		if(Input.GetKeyUp(KeyCode.Space))
		{
			if(state)
				StopCoroutine(LifeCycle());
			else
			{
				for(int y = 0; y < height; y++)
					for(int x = 0; x < width; x++)
						newColor[x + y * width] = srGreed[x + y * width].color;
				StartCoroutine(LifeCycle());
			}
			state = !state;
		}
		
		if(Input.GetKeyUp(KeyCode.C))
		{
			StopAllCoroutines();
			for(int y = 0; y < height; y++)
				for(int x = 0; x < width; x++)
					srGreed[x + y * width].color = Color.white;
		}
    }
	
	private IEnumerator LifeCycle()
    {
		while(true)
		{
			for(int y = 0; y < height; y++)
				for(int x = 0; x < width; x++)
				{
					int count = 0;
					int rowMin = y - 1 < 0 ? y : y - 1;
					int	rowMax = y + 1 == height ? y : y + 1;
					int colMin = x - 1 < 0 ? x : x - 1;
					int colMax = x + 1 == width ? x : x + 1;
					
					for(int i = rowMin; i <= rowMax; i++)
						for(int j = colMin; j <= colMax; j++)
						{
							if (i == y && j == x) continue;
							if (srGreed[j + i * width].color == Color.black) count++;
						}
					
					if(srGreed[x + y * width].color == Color.black && count < 2 || count > 3)
					{
						newColor[x + y * width] = Color.white;
					}
						
					else if(srGreed[x + y * width].color == Color.white && count == 3) 
					{
						newColor[x + y * width] = Color.black;
					}
				}
			for(int y = 0; y < height; y++)
					for(int x = 0; x < width; x++)
						srGreed[x + y * width].color = newColor[x + y * width];
			yield return new WaitForSeconds(1/simulationSpeed);
		}
	}
}
