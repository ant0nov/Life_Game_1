using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
	public SpriteRenderer sr;
	private Color colorBlack;
	private Color colorWhite;
	
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
		colorBlack = new Color(0f, 0f, 0f, 1f);
		colorWhite = new Color(1f, 1f, 1f, 1f);
    }

	void OnMouseOver()
	{
        if(Input.GetMouseButton(0)) sr.color = colorBlack;
		else if(Input.GetMouseButton(1)) sr.color = colorWhite;
    }
}
